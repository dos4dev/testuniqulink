<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    {{ Html::style('css/style.css') }}

    <title>Document</title>
</head>
<body>


@yield('content')
@if(Route::getCurrentRoute()->uri() == '/')
    <div class="container">
        <div class="row mt-3 d-flex justify-content-end ">
            <a class="btn btn-primary" href="/admin">Административная часть</a>
        </div>
        <div class="row">

        {!! Form::open(['route'=>'createlink']) !!}
            <label class="pt-5" for="user_name">Имя пользователя</label><br>
            <input class="form-control" type="text" name="user_name"><br>
            <label for="user_email form-control">Email</label><br>
            <input class="form-control" type="email" name="user_email" value="">
            <br>
            <input class="btn btn-success btn-block" type="submit" value="Подписаться">
            {!! Form::close() !!}
        </div>
        @foreach($data as $item)
            {{$item->user_name}}
            <br>
        @endforeach
    </div>
    </div>

@endif

</body>
</html>