@extends('main')

@section('content')
    <div class="container">
        <div class="row mb-3 mt-3">
            <a class="btn btn-primary" href="/">Главная страница</a>
        </div>
            <h2>{{$link->user_name}}, это Ваша уникальная страница.</h2>

            <h4>Cтрок действия ссылки закончиться <span class="text-danger">{{date('d.m.Y G:i:s', strtotime($link->created_at) + 7776000)}}</span></h4>
            {!! Form::model($link,['route'=>['Unsubcribe', $link->id], 'method'=>'DELETE']) !!}
            <input class="btn btn-danger" type="submit" value="Отписаться">
            {!! Form::close() !!}
    </div>

@endsection