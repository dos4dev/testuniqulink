@extends('main')

@section('content')
    <div class="container">
        <div class="row mb-3 mt-3">
            <a class="btn btn-primary" href="/">Главная страница</a>
        </div>
        <div class="row items ">
            <div class="id">
                Id:
            </div>
            <div class="user_name">
                Имя пользователя:
            </div>
            <div class="user_email">
                email:
            </div>
            <div class="unicLink">
                Ссылка:
            </div>
            <div class="created_at">
                Создана:
            </div>
            <div class="action">
                Действия
            </div>
        @foreach($items as $item)
            <div class="id">
                {{$item->id}}
            </div>
            <div class="user_name">
                {{$item->user_name}}
            </div>
            <div class="user_email">
                {{$item->user_email}}
            </div>
            <div class="unicLink">
                <a href="{{$item->unicLink}}">{{$item->unicLink}}</a>
            </div>
            <div class="created_at">
                {{$item->created_at}}
            </div>
            <div class="action">
                {!! Html::linkRoute('admin.edit', 'Редактировать', array($item->id), array('class' => 'btn btn-primary btn-block')) !!}
                {!! Form::open(['route' => ['admin.destroy', $item->id], 'method' => 'DELETE']) !!}

                {!! Form::submit('Удалить', ['class' => 'btn btn-danger btn-block']) !!}

                {!! Form::close() !!}
            </div>

        @endforeach
        </div>

    </div>

@endsection