@extends('main')

@section('content')
    {!! Form::model($link, ['route' => ['admin.update', $link->id], 'method' => 'PUT']) !!}

    <div class="container">
        <div class="row items ">
            <div class="id">
                Id:
            </div>
            <div class="user_name">
                Имя пользователя:
            </div>
            <div class="user_email">
                email:
            </div>
            <div class="unicLink">
                Ссылка:
            </div>
            <div class="created_at">
                Создана:
            </div>
            <div class="action">
                Действия
            </div>

            <div class="id">
                {{$link->id}}
            </div>
            <div class="user_name">
                <input type="text" name="user_name" value="{{$link->user_name}}">
            </div>
            <div class="user_email">
                <input type="email" name="user_email" value="{{$link->user_email}}">
            </div>
            <div class="unicLink">
                <input type="text" name="unicLink" value="{{$link->unicLink}}">
            </div>
            <div class="created_at">
                {{$link->created_at}}
            </div>
            <div class="action">

                {{ Form::submit('Сохранить', ['class' => 'btn btn-success btn-block']) }}
                {!! Form::close() !!}
                {!! Form::open(['route' => ['admin.destroy', $link->id], 'method' => 'DELETE']) !!}

                {!! Form::submit('Удалить', ['class' => 'btn btn-danger btn-block']) !!}

                {!! Form::close() !!}
            </div>

        </div>

    </div>


@endsection