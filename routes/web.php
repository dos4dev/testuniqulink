<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FludController@index');
Route::resource('/admin', 'AdminController');
Route::post('/createlink','UnicLinkController@index')->name('createlink');
Route::get('/{hash}', 'ShowUnicPageController@index');
Route::delete('/{id}', 'ShowUnicPageController@unsubscribe')->name('Unsubcribe');

