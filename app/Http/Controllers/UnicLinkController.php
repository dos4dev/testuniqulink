<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UnicLink;

class UnicLinkController extends Controller
{
    public  function __construct()
    {
        date_default_timezone_set('Europe/Kiev');
    }
    public function index(Request $request){

        $this->validate($request, array(
            'user_name'      =>  'required|max:255',
            'user_email' => 'unique:unic_links,user_email'

        ));
        $unicLink = new UnicLink();
        $unicLink->user_name = $request->user_name;
        $unicLink->user_email = $request->user_email;
        $unicLink->unicLink = 'link'. hash('crc32', $request->user_email);
        $unicLink->save();
        return view('custom.afterSubscribe')->with('unicLink', $unicLink);

    }

}
