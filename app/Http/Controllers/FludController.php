<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UnicLink;

class FludController extends Controller
{
    public function index(){
        $data = UnicLink::all();
        return view('main')->with(compact( 'data'));
    }
    public function flud(){

        $flud = new UnicLink();
        $flud->user_name = (string)date('Y-m-d G:i:s',time());
        $flud->save();

    }
}
