<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UnicLink;

class ShowUnicPageController extends Controller
{
    public function index($hash){
        if ($link = UnicLink::findByHash($hash)){
            $checkdate = strtotime($link->created_at) + 7776000;
            if (time() < $checkdate){
                return view('custom.uniquePage')->with('link', $link);
            }
            else{
                return "Строк действия ссылки истек, пожалуйста  зарегистрируйтесь заново.";
            }
        };

    }
    public function unsubscribe($id){

        $link = UnicLink::find($id);
        $link->delete();
        return redirect('/');
    }
}
