<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UnicLink;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.index', ['items'=> UnicLink::all()]);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $link = UnicLink::find($id);
        return view('admin.edit')->with('link', $link);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $link = UnicLink::find($id);
        $link->user_name = $request->user_name;
        $link->user_email = $request->user_email;
        $link->unicLink = $request->unicLink;
        $link->save();
        return redirect('admin');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $link = UnicLink::find($id);
        $link->delete();
        return redirect('admin');

    }
}
