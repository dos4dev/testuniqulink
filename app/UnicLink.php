<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UnicLink extends Model
{
    public static function findByHash($hash)
    {
        return DB::table('unic_links')->where('unicLink', $hash)->first();
    }
}
