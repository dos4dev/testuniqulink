<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\UnicLink;

class CronJob extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'CronJob:cronjob';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Date save successfully Response from protected method CronJob Class!';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $flud = new UnicLink();
        $flud->user_name = (string)date('Y-m-d G:i:s',time());
        $flud->save();

        $this->info('Date save Successfully Response From CronJob Class!');

    }
}
